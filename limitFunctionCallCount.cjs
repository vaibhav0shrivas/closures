function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let functionCallCounter = 0;
    if (typeof cb === 'function' && Number.isInteger(n) === true) {
        function callsCBnTimes() {
            if (functionCallCounter + 1 <= n) {
                functionCallCounter++;
                return cb();
            }
            else {
                return null;
            }
        }
        return callsCBnTimes;

    }
    return null;
    //No return value i.e undefined will be returned if invalid input.
}

module.exports = limitFunctionCallCount;