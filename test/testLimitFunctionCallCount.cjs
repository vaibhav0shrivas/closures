const testLimitFunctionCallCount = require('../limitFunctionCallCount.cjs');


function foo() {
    console.log("this is foo");
}

const boo = function booFunction() {
    console.log("this is boo");
}


const myFoo0 = testLimitFunctionCallCount(foo, 3);
const myFoo1 = testLimitFunctionCallCount(boo, 3);
const myFoo2 = testLimitFunctionCallCount(foo, 0);
const myFoo3 = testLimitFunctionCallCount(boo, 0);
const myFoo4 = testLimitFunctionCallCount(foo, -1);
const myFoo5 = testLimitFunctionCallCount(boo, -2);
const myFoo6 = testLimitFunctionCallCount(foo, '12');
const myFoo7 = testLimitFunctionCallCount(boo, []);
const myFoo8 = testLimitFunctionCallCount('ds', '3');
const myFoo9 = testLimitFunctionCallCount();
const myFoo10 = testLimitFunctionCallCount("boo", 3);

console.log("----------------------myFoo0 = testLimitFunctionCallCount(foo,3);");
console.log(myFoo0);
myFoo0();
myFoo0();
myFoo0();
myFoo0();//Nothing should happen here
myFoo0();//Nothing should happen here
console.log("----------------------myFoo1 = testLimitFunctionCallCount(boo,3);");
console.log(myFoo1);
myFoo1();
myFoo1();
myFoo1();
myFoo1();//Nothing should happen here
myFoo1();//Nothing should happen here
console.log("----------------------myFoo2 = testLimitFunctionCallCount(foo,0);");
console.log(myFoo2);
myFoo2();//Nothing should happen here
myFoo2();//Nothing should happen here
console.log("----------------------myFoo3 = testLimitFunctionCallCount(boo,0);");
console.log(myFoo3);
myFoo3();//Nothing should happen here
myFoo3();//Nothing should happen here
console.log("----------------------myFoo4 = testLimitFunctionCallCount(foo,-1);");
console.log(myFoo4);
myFoo4();//Nothing should happen here
myFoo4();//Nothing should happen here
console.log("----------------------myFoo5 = testLimitFunctionCallCount(boo,-2);");
console.log(myFoo5);
myFoo5();//Nothing should happen here
myFoo5();//Nothing should happen here
console.log("----------------------myFoo6 = testLimitFunctionCallCount(foo,'12');");
console.log(myFoo6);
console.log("----------------------myFoo7 = testLimitFunctionCallCount(boo,[]);");
console.log(myFoo7);
console.log("----------------------myFoo8 = testLimitFunctionCallCount('ds','3');");
console.log(myFoo8);
console.log("----------------------myFoo9 = testLimitFunctionCallCount();");
console.log(myFoo9);
console.log("----------------------myFoo10 = testLimitFunctionCallCount(\"boo\",3);");
console.log(myFoo10);


