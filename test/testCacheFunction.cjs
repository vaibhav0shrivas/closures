const testCacheFunction = require('../cacheFunction.cjs');


function fib(n, memo = {}) {
    if (n in memo) {
        return memo[n];
    }
    else if (n <= 2) {
        return 1;
    }
    memo[n] = fib(n - 1,memo) + fib(n - 2, memo);
    return memo[n];
}


const myfib0 = testCacheFunction(fib);
const myfib1 = testCacheFunction(fib, -2);
const myfib2 = testCacheFunction(fib, '12');
const myfib3 = testCacheFunction(fib, []);
const myfib4 = testCacheFunction('ds', '3');
const myfib5 = testCacheFunction();
const myfib6 = testCacheFunction("fib");

console.log("----------------------myfib1 = testCacheFunction(fib,-2);");
console.log("type of myfib1 ==>",typeof myfib1," myfib1 ==>",myfib1);


console.log("----------------------myfib2 = testCacheFunction(fib,'12');");
console.log("type of myfib2 ==>",typeof myfib2," myfib2 ==>",myfib2);


console.log("----------------------myfib3 = testCacheFunction(fib, []);");
console.log("type of myfib3 ==>",typeof myfib3," myfib3 ==>",myfib3);

console.log("----------------------myfib4 = testCacheFunction('ds','3');");
console.log("type of myfib4 ==>",typeof myfib4," myfib4 ==>",myfib4);

console.log("----------------------myfib5 = testCacheFunction();");
console.log("type of myfib5 ==>",typeof myfib5," myfib5 ==>",myfib5);

console.log("----------------------myfib6 = testCacheFunction('fib');");
console.log("type of myfib6 ==>",typeof myfib6," myfib6 ==>",myfib6);

console.log("----------------------myfib0 = testCacheFunction(fib);");
console.log("myfib0(5)==>",myfib0(5));
console.log("myfib0(7)==>",myfib0(7));
console.time('Execution Time 1st call with "13"');
console.log("myfib0(13)==>",myfib0(13));
console.timeEnd('Execution Time 1st call with "13"');
console.log("myfib0(2)==>",myfib0(2));

console.time('Execution Time 2nd call with "13"');
console.log("myfib0(13)==>",myfib0(13));
console.timeEnd('Execution Time 2nd call with "13"');

console.time('Execution Time 1st call with "18"');
console.log("myfib0(18)==>",myfib0(18));
console.timeEnd('Execution Time 1st call with "18"');

console.log("myfib0(5)==>",myfib0(5));

console.time('Execution Time 2nd call with "18"');
console.log("myfib0(18)==>",myfib0(18));
console.timeEnd('Execution Time 2nd call with "18"');
