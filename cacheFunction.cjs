function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    let cacheForCB = {};
    if (typeof cb === 'function' && arguments.length === 1) {
        function callsToCB(...args) {
            if (args in cacheForCB) {
                return cacheForCB[args];
            }
            else {
                let x = cb(...args);
                cacheForCB[args] = x;
                return x;
            }
        }
        return callsToCB;

    }
    return null;
}

module.exports = cacheFunction;